import cv2
import argparse
import numpy as np
import timeit
import cProfile
import glob
from tqdm import tqdm
import time

#This is for glob to select the videos
VideoPath = "Videos/*.*"
vid_number = 1

BackgroundSubtract = cv2.createBackgroundSubtractorKNN()

for filename in tqdm(glob.glob(VideoPath)):
    videoCapture = cv2.VideoCapture(filename)
    counter = 0

    width = int(videoCapture.get(3))
    height = int(videoCapture.get(4))
    saveFile = "ProcessedVideo/ProcessedCow" + str(vid_number) + ".mp4"

    saveVid = cv2.VideoWriter(saveFile, cv2.VideoWriter_fourcc(*'MP42'), 30.0, (width, height))

    while videoCapture.isOpened():
        ret, frame = videoCapture.read()
        if ret:
            # get the first frame and show it
            if counter == 0:
                backgroundImage = frame
                counter += 1

            #Background subtraction
            differences1 = cv2.subtract(frame, backgroundImage)
            differences2 = cv2.subtract(backgroundImage, frame)
            finalDiff = differences1 + differences2
            finalDiff[abs(finalDiff) < 30.0] = 0
            grayFinal = cv2.cvtColor(finalDiff, cv2.COLOR_BGR2GRAY)
            kernel = np.ones((3,3), np.uint8)
            grayFinal = cv2.erode(grayFinal, kernel, iterations=2)
            grayFinal = cv2.dilate(grayFinal, kernel, iterations=1)
            grayFinal[grayFinal > 5] = 255
            #########################

            #Save the video to the file
            saveVid.write(grayFinal)
            # cv2.imshow("Original", frame )
            # cv2.imshow("Final", grayFinal)

            if cv2.waitKey(25) & 0xFF == ord('q'):
                break
        else:
            break

    vid_number += 1


videoCapture.release()
saveVid.release()
cv2.destroyAllWindows()
